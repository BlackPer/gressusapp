angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      .state('menu.home', {
    url: '/page1',
    views: {
      'side-menu21': {
        templateUrl: 'templates/home.html',
        controller: 'homeCtrl'
      }
    }
  })

  .state('menu.myData', {
    url: '/page2',
    views: {
      'side-menu21': {
        templateUrl: 'templates/myData.html',
        controller: 'myDataCtrl'
      }
    }
  })

  .state('menu.medUser', {
    url: '/Med user',
    views: {
      'side-menu21': {
        templateUrl: 'templates/medUser.html',
        controller: 'medUserCtrl'
      }
    }
  })

  .state('menu', {
    url: '/side-menu21',
    templateUrl: 'templates/menu.html',
    controller: 'menuCtrl'
  })

  .state('menu.login', {
    url: '/page4',
    views: {
      'side-menu21': {
        templateUrl: 'templates/login.html',
        controller: 'loginCtrl'
      }
    }
  })

  .state('menu.signup', {
    url: '/page5',
    views: {
      'side-menu21': {
        templateUrl: 'templates/signup.html',
        controller: 'signupCtrl'
      }
    }
  })

  .state('menu.helpAndDiscover', {
    url: '/Help and discover',
    views: {
      'side-menu21': {
        templateUrl: 'templates/helpAndDiscover.html',
        controller: 'helpAndDiscoverCtrl'
      }
    }
  })

  .state('menu.contactUs', {
    url: '/Contact us',
    views: {
      'side-menu21': {
        templateUrl: 'templates/contactUs.html',
        controller: 'contactUsCtrl'
      }
    }
  })

  .state('menu.medLogin', {
    url: '/page9',
    views: {
      'side-menu21': {
        templateUrl: 'templates/medLogin.html',
        controller: 'medLoginCtrl'
      }
    }
  })

  .state('menu.products', {
    url: '/page10',
    views: {
      'side-menu21': {
        templateUrl: 'templates/products.html',
        controller: 'productsCtrl'
      }
    }
  })

$urlRouterProvider.otherwise('/side-menu21/page4')


});